helllo
<html>
  <head>
    <script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>
    <script type="text/javascript">
      google.charts.load('current', {packages:['wordtree']});
      google.charts.setOnLoadCallback(drawSimpleNodeChart);
      function drawSimpleNodeChart() {
        var data = google.visualization.arrayToDataTable(
          [  ['phrae','size',{ role: 'style' }],
['We are OSSPHINC',15,'blue'],
['OSSPHINC Mission',9,'red'],
['OSSPHINC Vision',9,'red'],
['Mission is to provide our client with a better solutions ',1,'green'],
['Vision to be known as the most reliable business solutions service provider',1,'green'],
['are group of innovative young professionals dedicated to provide business process solutions',1,'green'],
          ]
        );

        var options = {
          wordtree: {
            format: 'implicit',
            type: 'double',
            word: 'We',
            colors: ['red', 'black', 'green']
          }
        };

        var wordtree = new google.visualization.WordTree(document.getElementById('wordtree_double'));
        wordtree.draw(data, options);
      }
    </script>
  </head>
  <body>
    <div id="wordtree_double" style="width: 1500px; height: 500px;"></div>
  </body>
</html>